<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Contract\Entity;

interface UuidableInterface
{
    public function setUuid(string $uuid): void;

    public function getUuid(): ?string;

    public function generateUuid(): string;
}
