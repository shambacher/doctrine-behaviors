<?php

namespace Shambacher\DoctrineBehaviors\Contract\Entity;

use DateTimeImmutable;

interface SoftDeletableInterface
{
    public function delete(): void;

    public function restore(): void;

    public function isDeleted(): bool;

    /**
     * Checks whether the entity will be deleted.
     */
    public function willBeDeleted(?DateTimeImmutable $deletedAt = null): bool;

    public function getDeletedAt(): ?DateTimeImmutable;

    public function setDeletedAt(DateTimeImmutable $deletedAt): void;
}
