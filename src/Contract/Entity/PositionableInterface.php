<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Contract\Entity;

interface PositionableInterface
{
    public function getPosition(): int;
    public function setPosition(int $position): self;
}
