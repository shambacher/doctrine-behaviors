<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Contract\Entity;

use DateTimeImmutable;

interface TimestampableInterface
{
    public function updateTimestamps(): void;

    public function getCreatedAt(): ?DateTimeImmutable;

    public function setCreatedAt(DateTimeImmutable $createdAt): static;

    public function getUpdatedAt(): ?DateTimeImmutable;

    public function setUpdatedAt(DateTimeImmutable $updatedAt): static;
}
