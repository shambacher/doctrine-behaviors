<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Contract\Entity;

interface TokenableInterface
{
    public function setToken(string $token): void;
    public function getToken(): ?string;
    public function generateToken(): string;
    public function getTokenLength(): int;
    public function getTokenChars(): array;
}
