<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Exception;

use Exception;

final class SluggableException extends Exception
{
}
