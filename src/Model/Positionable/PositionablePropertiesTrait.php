<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Positionable;

use Doctrine\ORM\Mapping as ORM;

trait PositionablePropertiesTrait
{
    #[ORM\Column]
    private ?int $position = null;
}
