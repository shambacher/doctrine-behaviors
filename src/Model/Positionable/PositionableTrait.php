<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Positionable;

trait PositionableTrait
{
    use PositionablePropertiesTrait;
    use PositionableMethodsTrait;
}
