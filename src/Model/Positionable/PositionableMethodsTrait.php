<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Positionable;

trait PositionableMethodsTrait
{
    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
