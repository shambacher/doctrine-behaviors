<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Timestampable;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait TimestampablePropertiesTrait
{
    #[ORM\Column]
    protected ?DateTimeImmutable $createdAt = null;
    #[ORM\Column]
    protected ?DateTimeImmutable $updatedAt = null;
}
