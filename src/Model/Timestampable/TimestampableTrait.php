<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Timestampable;

trait TimestampableTrait
{
    use TimestampablePropertiesTrait;
    use TimestampableMethodsTrait;
}
