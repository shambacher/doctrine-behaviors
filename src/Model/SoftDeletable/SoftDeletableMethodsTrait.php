<?php

namespace Shambacher\DoctrineBehaviors\Model\SoftDeletable;

use Shambacher\DoctrineBehaviors\Exception\ShouldNotHappenException;
use DateTimeImmutable;
use DateTimeZone;

trait SoftDeletableMethodsTrait
{
    public function delete(): void
    {
        $this->deletedAt = $this->currentDateTime();
    }

    /**
     * Restore entity by undeleting it
     */
    public function restore(): void
    {
        $this->deletedAt = null;
    }

    public function isDeleted(): bool
    {
        if ($this->deletedAt !== null) {
            return $this->deletedAt <= $this->currentDateTime();
        }

        return false;
    }

    public function willBeDeleted(?DateTimeImmutable $deletedAt = null): bool
    {
        if ($this->deletedAt === null) {
            return false;
        }

        if ($deletedAt === null) {
            return true;
        }

        return $this->deletedAt <= $deletedAt;
    }

    public function getDeletedAt(): ?DateTimeImmutable
    {
        return $this->deletedAt;
    }

    public function setDeletedAt(?DateTimeImmutable $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    private function currentDateTime(): DateTimeImmutable
    {
        $dateTime = DateTimeImmutable::createFromFormat('U.u', sprintf('%.6F', microtime(true)));
        if ($dateTime === false) {
            throw new ShouldNotHappenException();
        }

        $dateTime = $dateTime->setTimezone(new DateTimeZone(date_default_timezone_get()));

        return $dateTime;
    }
}
