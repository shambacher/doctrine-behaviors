<?php

namespace Shambacher\DoctrineBehaviors\Model\SoftDeletable;

use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

trait SoftDeletablePropertiesTrait
{
    #[ORM\Column(nullable: true)]
    protected ?DateTimeImmutable $deletedAt = null;
}
