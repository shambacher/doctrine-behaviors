<?php

namespace Shambacher\DoctrineBehaviors\Model\SoftDeletable;

trait SoftDeletableTrait
{
    use SoftDeletablePropertiesTrait;
    use SoftDeletableMethodsTrait;
}
