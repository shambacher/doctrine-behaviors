<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Uuidable;

use Doctrine\ORM\Mapping as ORM;

trait UuidablePropertiesTrait
{
    #[ORM\Column(length: 255)]
    protected ?string $uuid = null;
}
