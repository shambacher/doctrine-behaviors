<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Uuidable;

use Shambacher\DoctrineBehaviors\Exception\ShouldNotHappenException;

trait UuidableMethodsTrait
{
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getUuid(): ?string
    {
        if (is_string($this->uuid)) {
            if ($this->uuid === '') {
                throw new ShouldNotHappenException();
            }
        }

        return $this->uuid;
    }

    public function generateUuid(): string
    {
        if ($this->uuid) {
            return $this->uuid;
        }

        $uuid = uniqid();
        $this->uuid = $uuid;
        return $uuid;
    }
}
