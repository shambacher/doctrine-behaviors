<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Uuidable;

trait UuidableTrait
{
    use UuidablePropertiesTrait;
    use UuidableMethodsTrait;
}
