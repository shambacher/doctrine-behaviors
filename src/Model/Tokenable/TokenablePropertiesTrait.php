<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Tokenable;

use Doctrine\ORM\Mapping as ORM;

trait TokenablePropertiesTrait
{
    #[ORM\Column(length: 255)]
    protected ?string $token = null;
}
