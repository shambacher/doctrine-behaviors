<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Tokenable;

trait TokenableTrait
{
    use TokenablePropertiesTrait;
    use TokenableMethodsTrait;
}
