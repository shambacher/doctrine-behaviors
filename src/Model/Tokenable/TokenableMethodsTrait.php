<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Tokenable;

use Shambacher\DoctrineBehaviors\Exception\ShouldNotHappenException;

trait TokenableMethodsTrait
{
    public function setToken(string $token): void
    {
        $this->token = $token;
    }

    public function getToken(): ?string
    {
        if (is_string($this->token)) {
            if ($this->token === '') {
                throw new ShouldNotHappenException();
            }
        }

        return $this->token;
    }

    public function generateToken(): string
    {
        if ($this->token) {
            return $this->token;
        }

        $tokenLength = $this->getTokenLength();
        if ($tokenLength <= 0) {
            throw new ShouldNotHappenException('Token length must be greater than 0');
        }
        if ($this->getTokenChars() <= 1) {
            throw new ShouldNotHappenException('Token chars must have at least 2 different characters');
        }

        $token = '';
        $tokenChars = $this->getTokenChars();
        $tokenCharsLength = count($tokenChars) - 1;
        for ($i = 0; $i < $this->getTokenLength(); $i++) {
            $index = random_int(0, $tokenCharsLength);
            $token .= $tokenChars[$index];
        }

        $this->token = $token;
        return $token;
    }

    public function getTokenLength(): int
    {
        return 50;
    }

    public function getTokenChars(): array
    {
        $composition = array_unique([
            'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
            'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            '0','1','2','3','4','5','6','7','8','9'
        ]);
        shuffle($composition);

        return $composition;
    }
}
