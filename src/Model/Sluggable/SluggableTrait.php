<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Sluggable;

trait SluggableTrait
{
    use SluggablePropertiesTrait;
    use SluggableMethodsTrait;
}
