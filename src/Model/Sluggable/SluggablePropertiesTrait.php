<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Model\Sluggable;

use Doctrine\ORM\Mapping as ORM;

trait SluggablePropertiesTrait
{
    #[ORM\Column(length: 255)]
    protected ?string $slug = null;
}
