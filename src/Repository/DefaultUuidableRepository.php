<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Repository;

use Shambacher\DoctrineBehaviors\Contract\Entity\UuidableInterface;
use Doctrine\ORM\EntityManagerInterface;

final class DefaultUuidableRepository
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function isUuidUniqueFor(UuidableInterface $uuidable, string $uniqueUuid): bool
    {
        $entityClass = $uuidable::class;

        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('COUNT(e)')
            ->from($entityClass, 'e')
            ->andWhere('e.uuid = :uuid')
            ->setParameter('uuid', $uniqueUuid);

        $identifiers = $this->entityManager->getClassMetadata($entityClass)->getIdentifierValues($uuidable);

        foreach ($identifiers as $field => $value) {
            if ($value === null || $field === 'uuid') {
                continue;
            }

            $normalizedField = str_replace('.', '_', $field);

            $queryBuilder
                ->andWhere(sprintf('e.%s != :%s', $field, $normalizedField))
                ->setParameter($normalizedField, $value);
        }

        return ! (bool) $queryBuilder->getQuery()
            ->getSingleScalarResult();
    }
}
