<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Repository;

use Shambacher\DoctrineBehaviors\Contract\Entity\TokenableInterface;
use Doctrine\ORM\EntityManagerInterface;

final class DefaultTokenableRepository
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function isTokenUniqueFor(TokenableInterface $tokenable, string $uniqueToken): bool
    {
        $entityClass = $tokenable::class;

        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('COUNT(e)')
            ->from($entityClass, 'e')
            ->andWhere('e.token = :token')
            ->setParameter('token', $uniqueToken);

        $identifiers = $this->entityManager->getClassMetadata($entityClass)->getIdentifierValues($tokenable);

        foreach ($identifiers as $field => $value) {
            if ($value === null || $field === 'token') {
                continue;
            }

            $normalizedField = str_replace('.', '_', $field);

            $queryBuilder
                ->andWhere(sprintf('e.%s != :%s', $field, $normalizedField))
                ->setParameter($normalizedField, $value);
        }

        return ! (bool) $queryBuilder->getQuery()
            ->getSingleScalarResult();
    }
}
