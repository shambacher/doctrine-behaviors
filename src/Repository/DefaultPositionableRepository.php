<?php

namespace Shambacher\DoctrineBehaviors\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Shambacher\DoctrineBehaviors\Contract\Entity\PositionableInterface;

final class DefaultPositionableRepository
{
    public function __construct(private readonly EntityManagerInterface $entityManager)
    {
    }

    public function findEqualAndUpperThan(PositionableInterface $positionable): array
    {
        $entityClass = $positionable::class;

        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from($entityClass, 'e')
            ->where('e.position >= :position')
            ->setParameter('position', $positionable->getPosition());

        return $queryBuilder->getQuery()->getResult();
    }

    public function findEqualThan(PositionableInterface $positionable): ?PositionableInterface
    {
        $entityClass = $positionable::class;

        $queryBuilder = $this->entityManager->createQueryBuilder()
            ->select('e')
            ->from($entityClass, 'e')
            ->where('e.position = :position')
            ->setParameter('position', $positionable->getPosition());

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
