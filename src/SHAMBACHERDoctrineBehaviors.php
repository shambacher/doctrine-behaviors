<?php

namespace Shambacher\DoctrineBehaviors;

use Shambacher\DoctrineBehaviors\DependencyInjection\SHAMBACHERDoctrineBehaviorsExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class SHAMBACHERDoctrineBehaviors extends AbstractBundle
{
    public function getPath(): string
    {
        return dirname(__DIR__);
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new SHAMBACHERDoctrineBehaviorsExtension();
    }
}
