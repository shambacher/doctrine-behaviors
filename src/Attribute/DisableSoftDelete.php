<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\Attribute;

#[\Attribute(\Attribute::TARGET_METHOD)]
final class DisableSoftDelete
{}
