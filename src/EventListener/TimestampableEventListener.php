<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Shambacher\DoctrineBehaviors\Contract\Entity\TimestampableInterface;

#[AsDoctrineListener(event: Events::prePersist, priority: 500, connection: 'default')]
#[AsDoctrineListener(event: Events::preUpdate, priority: 500, connection: 'default')]
final class TimestampableEventListener
{
    public function prePersist(PrePersistEventArgs $args): void
    {
        $this->execute($args);
    }

    public function preUpdate(PreUpdateEventArgs $args): void
    {
        $this->execute($args);
    }

    private function execute(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof TimestampableInterface) {
            return;
        }

        $entity->updateTimestamps();
    }
}
