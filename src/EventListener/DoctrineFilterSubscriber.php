<?php

declare(strict_types = 1);

namespace Shambacher\DoctrineBehaviors\EventListener;

use Shambacher\DoctrineBehaviors\Attribute\DisableSoftDelete;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionClass;
use ReflectionMethod;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class DoctrineFilterSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private EntityManagerInterface $entityManager,
    ) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => 'onKernelController',
        ];
    }

    public function onKernelController(ControllerEvent $event): void
    {
        $controller = $event->getController();

        if (!is_array($controller)) {
            return;
        }

        $controllerClass = new ReflectionClass($controller[0]);
        $method = $controllerClass->getMethod($controller[1]);

        $this->disableFiltersFromAttributes($method);
    }

    private function disableFiltersFromAttributes(ReflectionMethod $method): void
    {
        $attributes = $method->getAttributes(DisableSoftDelete::class);
        if (count($attributes) > 0) {
            $this->entityManager->getFilters()->disable('softDeletable');
        }
    }
}
