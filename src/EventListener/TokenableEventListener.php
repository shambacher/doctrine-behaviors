<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\EventListener;

use Shambacher\DoctrineBehaviors\Contract\Entity\SluggableInterface;
use Shambacher\DoctrineBehaviors\Contract\Entity\TokenableInterface;
use Shambacher\DoctrineBehaviors\Repository\DefaultTokenableRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;

#[AsDoctrineListener(event: Events::prePersist, priority: 500, connection: 'default')]
final class TokenableEventListener
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly DefaultTokenableRepository $defaultTokenableRepository
    )
    {}

    public function prePersist(PrePersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof TokenableInterface) {
            return;
        }

        $entity->generateToken();
        $this->generateUniqueTokenFor($entity);
    }

    private function generateUniqueTokenFor(TokenableInterface $tokenable): void
    {
        $i = 0;
        $token = $tokenable->getToken();

        $uniqueToken = $token;

        while (! (
            $this->defaultTokenableRepository->isTokenUniqueFor($tokenable, $uniqueToken)
            && $this->isTokenUniqueInUnitOfWork($tokenable, $uniqueToken)
        )) {
            $uniqueToken = $tokenable->generateToken();
        }

        $tokenable->setToken($uniqueToken);
    }

    private function isTokenUniqueInUnitOfWork(TokenableInterface $tokenable, string $uniqueToken): bool
    {
        $scheduledEntities = $this->getOtherScheduledEntities($tokenable);
        foreach ($scheduledEntities as $scheduledEntity) {
            if ($scheduledEntity->getToken() === $uniqueToken) {
                return false;
            }
        }

        return true;
    }

    private function getOtherScheduledEntities(TokenableInterface $tokenable): array
    {
        $unitOfWork = $this->entityManager->getUnitOfWork();

        $uowScheduledEntities = [
            ...$unitOfWork->getScheduledEntityInsertions(),
            ...$unitOfWork->getScheduledEntityUpdates(),
            ...$unitOfWork->getScheduledEntityDeletions(),
        ];

        $scheduledEntities = [];
        foreach ($uowScheduledEntities as $uowScheduledEntity) {
            if ($uowScheduledEntity instanceof SluggableInterface && $tokenable !== $uowScheduledEntity) {
                $scheduledEntities[] = $uowScheduledEntity;
            }
        }

        return $scheduledEntities;
    }
}
