<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\EventListener;

use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use Shambacher\DoctrineBehaviors\Contract\Entity\PositionableInterface;
use Shambacher\DoctrineBehaviors\Repository\DefaultPositionableRepository;

#[AsDoctrineListener(event: Events::prePersist, priority: 500, connection: 'default')]
final class PositionableEventListener
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly DefaultPositionableRepository $defaultPositionableRepository
    )
    {}

    public function prePersist(PrePersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof PositionableInterface) {
            return;
        }

        $toReset = $this->defaultPositionableRepository->findEqualAndUpperThan($entity);
        /** @var PositionableInterface $item */
        foreach($toReset as $index => $item) {
            $item->setPosition($entity->getPosition() + 1 * ($index + 1));
            $this->entityManager->persist($item);
        }
    }
}
