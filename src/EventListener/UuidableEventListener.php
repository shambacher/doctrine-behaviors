<?php

declare(strict_types=1);

namespace Shambacher\DoctrineBehaviors\EventListener;

use Shambacher\DoctrineBehaviors\Contract\Entity\UuidableInterface;
use Shambacher\DoctrineBehaviors\Repository\DefaultUuidableRepository;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsDoctrineListener;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\PrePersistEventArgs;
use Doctrine\ORM\Events;
use Shambacher\DoctrineBehaviors\Contract\Entity\SluggableInterface;

#[AsDoctrineListener(event: Events::prePersist, priority: 500, connection: 'default')]
final class UuidableEventListener
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly DefaultUUidableRepository $defaultUuidableRepository
    )
    {}

    public function prePersist(PrePersistEventArgs $args): void
    {
        $entity = $args->getObject();

        if (!$entity instanceof UuidableInterface) {
            return;
        }

        $entity->generateUuid();
        $this->generateUniqueUuidFor($entity);
    }

    private function generateUniqueUuidFor(UuidableInterface $uuidable): void
    {
        $i = 0;
        $uuid = $uuidable->getUuid();

        $uniqueUuid = $uuid;

        while (! (
            $this->defaultUuidableRepository->isUuidUniqueFor($uuidable, $uniqueUuid)
            && $this->isUuidUniqueInUnitOfWork($uuidable, $uniqueUuid)
        )) {
            $uniqueUuid = $uuidable->generateUuid();
        }

        $uuidable->setUuid($uniqueUuid);
    }

    private function isUuidUniqueInUnitOfWork(UuidableInterface $uuidable, string $uniqueUuid): bool
    {
        $scheduledEntities = $this->getOtherScheduledEntities($uuidable);
        foreach ($scheduledEntities as $scheduledEntity) {
            if ($scheduledEntity->getUuid() === $uniqueUuid) {
                return false;
            }
        }

        return true;
    }

    private function getOtherScheduledEntities(UuidableInterface $uuidable): array
    {
        $unitOfWork = $this->entityManager->getUnitOfWork();

        $uowScheduledEntities = [
            ...$unitOfWork->getScheduledEntityInsertions(),
            ...$unitOfWork->getScheduledEntityUpdates(),
            ...$unitOfWork->getScheduledEntityDeletions(),
        ];

        $scheduledEntities = [];
        foreach ($uowScheduledEntities as $uowScheduledEntity) {
            if ($uowScheduledEntity instanceof SluggableInterface && $uuidable !== $uowScheduledEntity) {
                $scheduledEntities[] = $uowScheduledEntity;
            }
        }

        return $scheduledEntities;
    }
}
