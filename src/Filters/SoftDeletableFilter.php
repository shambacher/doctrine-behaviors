<?php

namespace Shambacher\DoctrineBehaviors\Filters;

use Shambacher\DoctrineBehaviors\Contract\Entity\SoftDeletableInterface;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Query\Filter\SQLFilter;

class SoftDeletableFilter extends SQLFilter
{
    public function addFilterConstraint(ClassMetadata $targetEntity, string $targetTableAlias): string
    {
        if (!$targetEntity->getReflectionClass()->implementsInterface(SoftDeletableInterface::class)) {
            return '';
        }
        return sprintf('%s.deleted_at IS NULL or %s.deleted_at > CURRENT_TIMESTAMP()', $targetTableAlias, $targetTableAlias);
    }
}
